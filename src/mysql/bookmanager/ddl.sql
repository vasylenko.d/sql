CREATE TABLE `BOOKS` (
                         `ID` int(11) NOT NULL AUTO_INCREMENT,
                         `BOOK_TITLE` varchar(255) NOT NULL,
                         `BOOK_AUTHOR` varchar(255) NOT NULL,
                         `BOOK_ISBN` bigint(13) NOT NULL,
                         `BOOK_PRICE` varchar(255) NOT NULL,
                         PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8